#include "countdown.h"

CountDown::CountDown(QWidget *parent)
    : QObject{parent}
{ }

void CountDown::startTimer()
{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &CountDown::countTime);
    timer->start(100);

    countTime();
}

void CountDown::stopTimer()
{
    timer->stop();
}

void CountDown::resetTimer()
{
    count = 100;
}

void CountDown::countTime()
{
    count--;
    emit tick();
    if (count == 0)
    {
        emit reachedZero();
        return;
    }

}

int CountDown::getTime()
{
    return count;
}
