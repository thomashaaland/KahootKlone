# Tahaat

Tahaat is a [Kahoot](kahoot.it) clone with some of the same functionality. The game asks some questions and the player need to answer before the timer is up. The quicker the player answers the more points the player gets.

## Install and run
To run you need to run it through [Qt](qt.io). 

## Run examples
The main screen
![](images/introScreen.PNG)

Example of gameplay
![](images/gamePlay.PNG)
