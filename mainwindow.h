#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QPropertyAnimation>
#include <QStringListModel>
#include "quizengine.h"
#include "countdown.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void updateAnswerCorrectness();
    void updateQuizPage();
    void goToLastPage();
    void restartGame();

private slots:
    void on_enterUNameEdit_textChanged();
    void incrementCountDownClockColor();
    void on_quitButton_clicked();
    void on_highScoreButton_clicked();
    void on_menuButton_clicked();

private:
    Ui::MainWindow *ui;
    QuizEngine *quizEngine;
    CountDown *countDowner;
    QPropertyAnimation *resultAnimation;
    QPropertyAnimation *answerAnimation;
    QStringListModel *nameModel;
    QStringListModel *scoreModel;

    int applicationStatus;
    QStringList applicationStatusDescription;
    QStringList highScoresScore;
    QStringList highScoresName;
    void setQuestionStatus();
    QString neutralStyle;
    QString correctStyle;
    QString wrongStyle;
    void initQuizPage();
    void loadScores();
    void saveHighScore();
};
#endif // MAINWINDOW_H
