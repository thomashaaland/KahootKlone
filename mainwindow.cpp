#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , quizEngine(new QuizEngine)
    , countDowner(new CountDown)
    , resultAnimation(nullptr)
    , answerAnimation(nullptr)
    , nameModel(nullptr)
    , scoreModel(nullptr)
{
    // Grab the styles etc.
    QFile fileNeutral(":/css/neutralStyle.css");
    QFile fileCorrect(":/css/correctStyle.css");
    QFile fileWrong(":/css/wrongStyle.css");
    fileNeutral.open(QFile::ReadOnly);
    fileCorrect.open(QFile::ReadOnly);
    fileWrong.open(QFile::ReadOnly);
    neutralStyle = QLatin1String(fileNeutral.readAll());
    correctStyle = QLatin1String(fileCorrect.readAll());
    wrongStyle = QLatin1String(fileWrong.readAll());


    // Initiate the application status
    applicationStatus = 0;
    applicationStatusDescription = { "Timeout!", "Correct!", "Wrong!" };

    // Setting up all pages
    ui->setupUi(this);
    loadScores();

    // Setting up styles for all widgets
    ui->pageStacker->setCurrentIndex(0);
    qApp->setStyleSheet(neutralStyle);

    // connects in Login page
    // Make it so that pressing the startbutton picks up what is entered into name and assigns name to name label on next page
    connect(ui->startButton, &QPushButton::clicked, quizEngine, [=](){ quizEngine->setPlayerName(ui->enterUNameEdit->toPlainText()); });
    connect(ui->startButton, &QPushButton::clicked, ui->welcomeLabel, [=](){
        ui->welcomeLabel->setText(
                    "Welcome, " + quizEngine->getPlayerName());
    });
    // Make it so pressing the startbutton switch to the quiz page
    connect(ui->startButton, &QPushButton::clicked, ui->pageStacker, [=](){
        updateQuizPage();
        ui->pageStacker->setCurrentIndex(1);
    });

    // Connects in quiz page
    connect(ui->answer1_button, &QPushButton::clicked, quizEngine, [=](){
        quizEngine->reportAnswer(ui->answer1_button->text());
    });
    connect(ui->answer2_button, &QPushButton::clicked, quizEngine, [=](){
        quizEngine->reportAnswer(ui->answer2_button->text());
    });
    connect(ui->answer3_button, &QPushButton::clicked, quizEngine, [=](){
        quizEngine->reportAnswer(ui->answer3_button->text());
    });
    connect(ui->answer4_button, &QPushButton::clicked, quizEngine, [=](){
        quizEngine->reportAnswer(ui->answer4_button->text());
    });

    connect(ui->nextQuestionButton, &QPushButton::clicked, quizEngine, [=](){
        quizEngine->incrementCurrentQuestion();
        this->updateQuizPage();
    });

    // Whenever the quizengine is done checking for answercorrectness activate slot in MainWindow
    connect(quizEngine, &QuizEngine::correctAnswer, this, &MainWindow::updateAnswerCorrectness);

    // When reaching the end of the questions, need to go to the end page
    connect(quizEngine, &QuizEngine::lastPage, this, &MainWindow::goToLastPage);
    connect(quizEngine, &QuizEngine::lastPage, this, &MainWindow::saveHighScore);

    // Connecting the timer with the display
    connect(countDowner, &CountDown::tick, ui->countDownClock, [=]() { ui->countDownClock->setValue(countDowner->getTime()); });
    connect(countDowner, &CountDown::tick, this, &MainWindow::incrementCountDownClockColor);
    connect(countDowner, &CountDown::reachedZero, this, &MainWindow::updateAnswerCorrectness);

    // Connect the restart button to a restart method
    connect(ui->restartButton, &QPushButton::clicked, this, &MainWindow::restartGame);

}

MainWindow::~MainWindow()
{
    delete ui;
    delete quizEngine;
    delete countDowner;
    if (resultAnimation) delete resultAnimation;
    if (answerAnimation) delete answerAnimation;
    if (nameModel) delete nameModel;
    if (scoreModel) delete scoreModel;
}


/**
 * @brief MainWindow::updateQuizPage
 * This runs when a new question is asked
 */
void MainWindow::updateQuizPage()
{
    quizEngine->resetEngineStatus();
    ui->scoreCounterLabel->setText(quizEngine->getScore());
    applicationStatus = quizEngine->getEngineStatus();
    ui->gameStatusLabel->setText(applicationStatusDescription[applicationStatus]);
    ui->resultLabel->setText(applicationStatusDescription[applicationStatus]);
    ui->questionLabel->setText(quizEngine->getQuestion());
    QStringList answer_suggestions = quizEngine->getPossibleAnswers();
    ui->answer1_button->setText(answer_suggestions[0]);
    ui->answer2_button->setText(answer_suggestions[1]);
    ui->answer3_button->setText(answer_suggestions[2]);
    ui->answer4_button->setText(answer_suggestions[3]);
    ui->answer1_button->setDisabled(false);
    ui->answer2_button->setDisabled(false);
    ui->answer3_button->setDisabled(false);
    ui->answer4_button->setDisabled(false);
    ui->resultLabel->setDisabled(true);
    ui->gameStatusLabel->setDisabled(true);
    ui->nextQuestionButton->setDisabled(true);

    ui->resultLabel->setStyleSheet(neutralStyle);
    ui->gameStatusLabel->setStyleSheet(neutralStyle);


    // Handle the timer
    countDowner->resetTimer();
    countDowner->startTimer();

    // Handle button styles
    setQuestionStatus();
}

/**
 * @brief MainWindow::updateAnswerCorrectness
 * This method runs when an answer has been given
 */
void MainWindow::updateAnswerCorrectness()
{
    countDowner->stopTimer();
    applicationStatus = quizEngine->getEngineStatus();
    if (applicationStatus == 1)
    {
        quizEngine->addScore(countDowner->getTime());
        ui->resultLabel->setStyleSheet(correctStyle);
        ui->gameStatusLabel->setStyleSheet(correctStyle);
    }
    else
    {
        ui->resultLabel->setStyleSheet(wrongStyle);
        ui->gameStatusLabel->setStyleSheet(wrongStyle);
    }
    ui->gameStatusLabel->setText(applicationStatusDescription[applicationStatus]);
    ui->resultLabel->setText(quizEngine->getAnswer());
    ui->scoreCounterLabel->setText(quizEngine->getScore());
    ui->answer1_button->setDisabled(true);
    ui->answer2_button->setDisabled(true);
    ui->answer3_button->setDisabled(true);
    ui->answer4_button->setDisabled(true);

    int width = this->size().width();
    int height = this->size().height();
    QRect endResultLabel = ui->resultLabel->geometry();
    QRect endGameStatusLabel = ui->gameStatusLabel->geometry();


    if (answerAnimation) delete answerAnimation;
    answerAnimation = new QPropertyAnimation(ui->gameStatusLabel, "geometry");
    answerAnimation->setDuration(100);
    answerAnimation->setStartValue(QRect(height, width / 2, 0, 0));
    answerAnimation->setEndValue(endGameStatusLabel);
    answerAnimation->start();

    if (resultAnimation) delete resultAnimation;
    resultAnimation = new QPropertyAnimation(ui->resultLabel, "geometry");
    resultAnimation->setDuration(100);
    resultAnimation->setStartValue(QRect(height, width / 2, 0, 0));
    resultAnimation->setEndValue(endResultLabel);
    resultAnimation->start();

    ui->resultLabel->setDisabled(false);
    ui->gameStatusLabel->setDisabled(false);
    ui->nextQuestionButton->setDisabled(false);
}

void MainWindow::setQuestionStatus()
{ }

void MainWindow::loadScores()
{
    QFile scores(qApp->applicationDirPath() + "\\scores.txt");
    scores.open(QFile::ReadOnly);
    QTextStream in(&scores);
    highScoresName.clear();
    highScoresScore.clear();
    while (!in.atEnd())
    {
        QString line = in.readLine();
        QStringList qList = line.split(';');
        highScoresName.append(qList[0]);
        highScoresScore.append(qList[1]);
    }
    if (nameModel != nullptr) delete nameModel;
    nameModel = new QStringListModel(this);
    if (scoreModel != nullptr) delete scoreModel;
    scoreModel = new QStringListModel(this);

    nameModel->setStringList(highScoresName);
    scoreModel->setStringList(highScoresScore);
    ui->highScoreNameList->setModel(nameModel);
    ui->highScoreScoreList->setModel(scoreModel);
}

void MainWindow::saveHighScore()
{
    highScoresName.append(quizEngine->getPlayerName());
    highScoresScore.append(quizEngine->getScore());
    std::vector<size_t> idx(highScoresName.size());
    std::iota( idx.begin(), idx.end(), 0);
    std::sort( idx.begin(), idx.end(), [&](int i, int j){return highScoresScore[i].toInt() > highScoresScore[j].toInt();});
    QStringList namesTemp;
    QStringList scoresTemp;
    for (auto i : idx)
    {
        namesTemp.append(highScoresName[i]);
        scoresTemp.append(highScoresScore[i]);
    }
    highScoresName = namesTemp;
    highScoresScore = scoresTemp;

    if (highScoresName.size() >= 5)
    {
        highScoresName.erase(highScoresName.begin() + 5, highScoresName.end());
        highScoresScore.erase(highScoresScore.begin() + 5, highScoresScore.end());
    }

    QFile scores(qApp->applicationDirPath() + "\\scores.txt");//":/scores/scores.txt");
    if (scores.open(QFile::ReadWrite | QFile::Truncate | QFile::Text))
    {
        QTextStream stream( &scores );
        for (int i = 0; i < highScoresName.size(); i++)
        {
            stream << highScoresName[i] << ";" << highScoresScore[i] << "\n";
        }
    }
}

/**
 * @brief MainWindow::goToLastPage
 * After all questions has been answered goes to the last page
 */
void MainWindow::goToLastPage()
{
    ui->pageStacker->setCurrentIndex(2);
    ui->finalScore->setText(quizEngine->getScore());
    ui->congratsLabel->setText("Congratulations, " + quizEngine->getPlayerName() + "!");
}

void MainWindow::restartGame()
{
    countDowner->stopTimer();
    quizEngine->reset();
    ui->enterUNameEdit->setText("");
    loadScores();
    ui->pageStacker->setCurrentIndex(0);
}

void MainWindow::on_enterUNameEdit_textChanged()
{
    int maxChar = 6;
    if (ui->enterUNameEdit->toPlainText().length() > maxChar)
    {
        int diff = ui->enterUNameEdit->toPlainText().length() - maxChar;
        QString newString = ui->enterUNameEdit->toPlainText();
        newString.chop(diff);
        ui->enterUNameEdit->setText(newString);
        QTextCursor cursor(ui->enterUNameEdit->textCursor());
        cursor.movePosition(QTextCursor::End, QTextCursor::MoveAnchor);
        ui->enterUNameEdit->setTextCursor(cursor);
    }
}

void MainWindow::incrementCountDownClockColor()
{
    int value = ui->countDownClock->value();

    QPalette pal = QPalette();
    pal.setColor(QPalette::Window, qRgb(2.55*(100 - value), 2.55 * value, 2.55*(value - (value * value)/100)));
    pal.setColor(QPalette::Button, qRgb(2.55*(100 - value), 2.55 * value, 2.55*(value - (value * value)/100)));
    pal.setColor(QPalette::ButtonText, qRgb(2.55*(100 - value), 2.55 * value, 2.55*(value - (value * value)/100)));
    pal.setColor(QPalette::WindowText, qRgb(2.55*(100 - value), 2.55 * value, 2.55*(value - (value * value)/100)));

    ui->countDownClock->autoFillBackground();
    ui->countDownClock->setPalette(pal);
    ui->countDownClock->show();
}


void MainWindow::on_quitButton_clicked()
{
    QApplication::quit();
}

void MainWindow::on_highScoreButton_clicked()
{
    ui->pageStacker->setCurrentIndex(3);
}


void MainWindow::on_menuButton_clicked()
{
    ui->pageStacker->setCurrentIndex(0);
}

