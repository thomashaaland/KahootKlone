#ifndef QUIZENGINE_H
#define QUIZENGINE_H

#include <QObject>
#include <QtDebug>
#include <algorithm>
#include <QFile>

class QuizEngine : public QObject
{
    Q_OBJECT
public:
    explicit QuizEngine(QObject *parent = nullptr);
    QString getPlayerName();
    QString getQuestion();
    QString getAnswer();
    QString getScore();
    void addScore(int scoreToAdd);
    int getEngineStatus();
    void resetEngineStatus();
    QStringList getPossibleAnswers();
    void reset();

signals:
    void correctAnswer();
    void lastPage();

public slots:
    void setPlayerName(QString name);
    void incrementCurrentQuestion();
    void decrementCurrentQuestion();
    void reportAnswer(QString answer);

private:
    QString playerName;
    QStringList questions;
    QList<QStringList> answerSuggestions;
    QStringList answerCorrect;
    void loadQuiz();
    int engineStatus;
    int currentQuestion;
    int score;
};

#endif // QUIZENGINE_H
