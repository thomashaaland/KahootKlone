#ifndef TIMER_H
#define TIMER_H

#include <QObject>
#include <QLCDNumber>

class Timer : public QLCDNumber
{
    Q_OBJECT
public:
    Timer(QWidget *parent = nullptr);

private slots:
    void showTime();
};

#endif // TIMER_H
