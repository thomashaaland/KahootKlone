#ifndef COUNTDOWN_H
#define COUNTDOWN_H

#include <QObject>
#include <QLCDNumber>
#include <QTimer>
#include <QTime>

class CountDown : public QObject
{
    Q_OBJECT
public:
    CountDown(QWidget *parent = nullptr);
    void startTimer();
    void stopTimer();
    void resetTimer();
    int getTime();

signals:
    void tick();
    void reachedZero();

public slots:
    void countTime();

private:
    int count;
    QTimer *timer;
};

#endif // COUNTDOWN_H
