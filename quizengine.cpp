#include "quizengine.h"

QuizEngine::QuizEngine(QObject *parent)
    : QObject{parent}
{
    currentQuestion = 0;
    score = 0;
    engineStatus = 0;
    loadQuiz();
}

void QuizEngine::loadQuiz()
{
    QFile quizes(":/quizes/quizes/quizes.txt");
    QFile quizSuggestions(":/quizes/quizes/answer_suggestions.txt");
    QFile quizAnswer(":/quizes/quizes/answer_correct.txt");
    if (quizes.open(QFile::ReadOnly))
    {
        QTextStream in(&quizes);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList qList = line.split(';');
            questions.append(qList);
        }
    }
    if (quizSuggestions.open(QFile::ReadOnly))
    {
        QTextStream in(&quizSuggestions);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList qList = line.split(';');
            answerSuggestions.append(qList);
        }
    }
    if (quizAnswer.open(QFile::ReadOnly))
    {
        QTextStream in(&quizAnswer);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            answerCorrect.append(line);
        }
    }
}

void QuizEngine::setPlayerName(QString name) {
    this->playerName = name;
}

QString QuizEngine::getPlayerName()
{
    return playerName;
}

QString QuizEngine::getQuestion()
{
    return questions[currentQuestion];
}

QString QuizEngine::getAnswer()
{
    return answerCorrect[currentQuestion];
}

QStringList QuizEngine::getPossibleAnswers()
{
    QStringList suggestions = answerSuggestions[currentQuestion];
    std::random_shuffle(suggestions.begin(), suggestions.end());
    return suggestions;
}

QString QuizEngine::getScore()
{
    return QString::number(score);
}

void QuizEngine::addScore(int scoreToAdd)
{
    score += scoreToAdd;
}

int QuizEngine::getEngineStatus()
{
    return engineStatus;
}

void QuizEngine::resetEngineStatus()
{
    engineStatus = 0;
}

void QuizEngine::incrementCurrentQuestion()
{
    if (currentQuestion == answerCorrect.size() - 1)
        emit lastPage();
    else
        currentQuestion++;
}

void QuizEngine::decrementCurrentQuestion()
{
    currentQuestion--;
}

void QuizEngine::reportAnswer(QString answer)
{
    if (answer == answerCorrect[currentQuestion])
    {
        engineStatus = 1;
    }
    else {
        engineStatus = 2;
    }
    emit correctAnswer();
}

void QuizEngine::reset()
{
    currentQuestion = 0;
    score = 0;
}
